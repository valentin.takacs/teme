let button;
let element;
let numbers = [];

function setup() {
  createCanvas(400, 400);
  element = createInput('');
  element.position(19, 19);
  button = createButton('Read element');
  button.position(19, 50);
  button.mousePressed(readElements);
}

function draw() {
  clear();
  background("#00ff99");
  if (numbers.length > 0) {
    textSize(18);
    fill('black');
    text("Citire array: ", 19, 90);
    text(numbers, 19, 110);
    text("Suma Numerelor: ", 19, 130);
    text(sumaNumerelor(numbers), 19, 150);
    text("Diferenta Numerelor: ", 19, 170);
    text(diferentaNumerelor(numbers), 19, 190);
    text("Produsul Numerelor: ", 19, 210);
    text(produsulNumerelor(numbers), 19, 230);
    text("Suma Numerelor Pare: ", 19, 250);
    text(sumaNumerePare(numbers), 19, 270);
    text("Diferenta Numerelor Inpare: ", 19, 290);
    text(diferentaNumereImpare(numbers), 19, 310);
    text("Suma celor mai mari doua numere: ", 19, 330);
    text(sumaNumereMari(numbers), 19, 350);
    fill(0, 102, 153);
  }
}

function readElements() {
  numbers.push(Number(element.value()));
  element.value('');
}

function sumaNumerelor(n) {
  let suma = 0;
  for (var i = 0; i < n.length; i++)
    suma = suma + n[i];
  return suma;
}

function diferentaNumerelor(n) {
  let diferenta = 0;
  for (var i = 0; i < n.length; i++)
    diferenta = diferenta - n[i];
  return diferenta;
}

function produsulNumerelor(n) {
  let produsul = 1;
  for (var i = 0; i < n.length; i++)
    produsul = produsul * n[i];
  return produsul;
}

function sumaNumerePare(n) {
  let sumaNrPare = 0;
  for (var i = 0; i < n.length; i++)
    if (n[i] % 2 == 0)
      sumaNrPare = sumaNrPare + n[i];
  return sumaNrPare;
}

function diferentaNumereImpare(n) {

  let diferentaNrImpare = 0;
  for (var i = 0; i < n.length; i++)
    if (n[i] % 2 != 0)
      diferentaNrImpare = diferentaNrImpare - n[i];

  return diferentaNrImpare;
}

function sumaNumereMari(n) {
  let sumaNrMari = n[0];
  for (var i = 0; i < n.length; i++) {
    for (var j = i + 1; j < n.length; j++)

      if (sumaNrMari < n[i] + n[j])
        sumaNrMari = n[i] + n[j];
  }
  return sumaNrMari;
}