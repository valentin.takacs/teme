var matrice1Inp;
var matrice2Inp;
var buttonCalcul;
var matrice1Dim;
var matrice2Dim;
var m1, m2, mr;

function setup() {
  createCanvas(400, 400);
  matrice1Inp = createInput();
  matrice1Inp.position(10, 10);
  matrice2Inp = createInput();
  matrice2Inp.position(10, 40);
  buttonCalcul = createButton("Calculează");
  buttonCalcul.style('red');
  buttonCalcul.position(200, 25);
  buttonCalcul.mousePressed(calcul);
  //matrix=new Matrix(i,j);
}

function draw() {
  background('green');
  if (m1 && m2) {
    m1.display(10, 110);
    m2.display(30 + m1.rowLength * 20, 110);
    mr.display(30 + m1.rowLength * 20 + 30 + m2.rowLength * 20, 110);
  }

}

function createRandomMatrix(i, j) {
  let matrix = new Matrix(i, j);
  for (let k = 0; k < j; k++) {
    let row = [];
    for (let l = 0; l < i; l++) {
      row.push(Math.floor(Math.random() * 10));
    }
    matrix.cells.push(row);
  }
  return m;
}

function calcul() {
  matrice1Dim = matrice1Input.value().split(',');
  matrice2Dim = matrice2Input.value().split(',');
  if (matrice1Dim[0] == matrice2Dim[1] && matrice1Dim[1] == matrice2Dim[0]) {
    m1 = createRandomMatrix(matrice1Dim[0], matrice1Dim[1]);
    m2 = createRandomMatrix(matrice2Dim[0], matrice2Dim[1]);
    mr = multiply(m1, m2);
  } else {
    confirm("Dimenisunea nu se ptriveste");
  }
}


function multiply(m1, m2) {
  let m1NumRows = m1.cells.length;
  let m1NumCols = m1.cells[0].length;
  let m2NumRows = m2.cells.length;
  let m2NumCols = m2.cells[0].length;
  let mrCells = new Array(m1NumRows);
  for (let r = 0; r < m1NumRows; ++r) {
    mrCells[r] = new Array(m2NumCols);
    for (let c = 0; c < m2NumCols; ++c) {
      mrCells[r][c] = 0;
      for (let i = 0; i < m1NumCols; ++i) {
        mrCells[r][c] += m1.cells[r][i] * m2.cells[i][c];
      }
    }
  }

  let result = new Matrix(m1NumRows, m2NumCols);
  result.cells = mrCells;
  return result;
}