class Matrix {
  constructor(rowLength, colLength) {
    this.rowLength = rowLength;
    this.colLength = colLength;
    this.cells = [];
  }

  display(x, y) {
    fill('ffff00');
    for (let i = 0; i < this.colLength; i++) {
      for (let j = 0; j < this.rowLength; j++) {
        text(String(this.cells[i][j]), x + j * 20, y + i * 20);
      }
    }
  }
}