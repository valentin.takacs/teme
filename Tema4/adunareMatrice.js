var matrice1 = [];
var matrice2 = [];
var sumaMatrice = [];

function setup() {
  fill('green');
  createCanvas(400, 400);
  input = createInput();
  var button = createButton('AdunareM1&M2');
  button.style('color', '#ff0000');
  button.mousePressed(createMatrice);
}

function draw() {
  background('green');
  fill('white');
  text("Matrice1",10 ,110);
  afisareMatrice(matrice1, 10, 20);
  text("Matrice2",110 ,110);
  afisareMatrice(matrice2, 110, 20);
  text("Suma",210 ,110);
  afisareMatrice(sumaMatrice, 210, 20);
}

function createMatrice() {
  matrice1 = [];
  matrice2 = [];
  sumaMatrice = [];
  dimensiuneMatrice = input.value();
  for (var i = 0; i < dimensiuneMatrice; i++) {
    matrice1.push([]);
    matrice2.push([]);
    for (var j = 0; j < dimensiuneMatrice; j++) {
      matrice1[i][j] = Math.floor(Math.random() * 10);
      matrice2[i][j] = Math.floor(Math.random() * 10);
    }
  }
  sumaMatrice = adunareMatrice(matrice1, matrice2);
}

function adunareMatrice(matrix1, matrix2) {
  let sumMatrix = [];
  for (var i = 0; i < matrix1.length; i++) {
    sumMatrix.push([]);
    for (var j = 0; j < matrix1.length; j++) {
      sumMatrix[i][j] = matrix1[i][j] + matrix2[i][j];
    }
  }
  return sumMatrix;
}

function afisareMatrice(m, x, y) {
  if (m.length != 0) {
    for (let i = 0; i < m.length; i++) {
      for (let j = 0; j < m.length; j++) {
        text(m[i][j], x + 15 * j, y + 15 * i);
      }
    }
  }
}